(function (e) {
    //header de conexão com o master data
    var header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-10',
        'Content-Type': 'application/json; charset=utf-8'
    };

    var insertMasterData = function (ENT, loja, dados, fn) {
        $.ajax({
            url: 'https://api.vtexcrm.com.br/' + loja + '/dataentities/' + ENT + '/documents/',
            type: 'PATCH',
            data: dados,
            headers: header,
            success: function (res) {
                fn(res);
            },
            error: function (res) {}
        });
    };

    var selectMasterData = function (ENT, loja, params, fn) {
        // Consulta dados na master data
        $.ajax({
            url: 'https://api.vtexcrm.com.br/' + loja + '/dataentities/' + ENT + '/search?' + params,
            type: 'GET',
            headers: header,
            success: function (res) {
                fn(res);
            },
            error: function (res) {}
        });
    };

    e(document).ready(function () {
        t.init();

        //QUANTIDADE DE PRODUTOS - CARRINHO
        function qtdNoCarrinho() {
            var quantidade = 0;

            vtexjs.checkout.getOrderForm()
                .done(function (orderForm) {
                    for (var i = orderForm.items.length - 1; i >= 0; i--) {
                        quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                    }

                    $('#cart div').text(quantidade);
                });
        }
        qtdNoCarrinho();

        //DEPARTAMENTO
        if ($('body.departamento').length > 0 || $('body.categoria').length > 0) {
            $(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch();
        };

        //PRODUTO
        if ($('body.produto').length === 1) {
            //QUANTIDADE DE PRODUTOS
            $('.quantity-size input[type="button"]').on('click', function (e) {
                e.preventDefault();
                if ($(this).hasClass('qtyplus')) {
                    var qtd_produtos = $('#Quantity').attr('value');
                    qtd_produtos++;
                } else if ($(this).hasClass('qtyminus')) {
                    var qtd_produtos = $('#Quantity').attr('value');

                    if (qtd_produtos > 1) {
                        qtd_produtos--;
                    }
                }

                $('#Quantity').attr('value', qtd_produtos);
            });

            vtexjs.checkout.getOrderForm()
                .done(function (orderForm) {
                    console.log(orderForm);
                });

            //COMPRAR PRODUTO
            $('#AddToCart').on('click', function (e) {
                e.preventDefault();
                vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {

                    var number = $('#Quantity').val();
                    var int = selectedToBuy[0];

                    if (int === undefined) {
                        int = product.skus[0].sku;
                    } else {
                        int = selectedToBuy[0];
                    }
                    console.log(int)

                    var item = {
                        id: int,
                        quantity: number,
                        seller: '1'
                    };

                    vtexjs.checkout.getOrderForm()
                    vtexjs.checkout.addToCart([item], null, 1)
                        .done(function (orderForm) {
                            console.log('Produto adicionado com sucesso!');
                            $('.ajax-success-modal.modal').show();
                            qtdNoCarrinho();
                        });
                });
            });
        }

        //ACCOUNT
        if ($('body').hasClass('account')) {
            function maskara () {
              $('#telefone-account').mask('(99) 9999-99990');
              $('#cpf-account').mask('000.000.000-00', {reverse: true});
              $('#data-account').mask('00/00/0000');
              $('#cnpj-account').mask('00.000.000/0000-00', {reverse: true});
            } maskara();
            
            function TestaCPF(strCPF) {
                var Soma;
                var Resto;
                Soma = 0;
              if (
                  strCPF == "00000000000" ||
                  strCPF == "11111111111" ||
                  strCPF == "22222222222" ||
                  strCPF == "33333333333" ||
                  strCPF == "44444444444" ||
                  strCPF == "55555555555" ||
                  strCPF == "66666666666" ||
                  strCPF == "77777777777" ||
                  strCPF == "88888888888" ||
                  strCPF == "99999999999" 
                ) return false;
                
              for (var i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
              Resto = (Soma * 10) % 11;
              
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
              
              Soma = 0;
                for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
              
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
                return true;
            }
        
            if ($.cookie('IPI') != undefined) {
              if($('.pessoa-fisica form').length){
                  var user = {
                    "async": true,
                    "crossDomain": true,
                    "url": "https://api.vtex.com/mykhome/dataentities/CL/search?_fields=email,birthDate,gender,firstName,lastName,document,phone,cnpj,stateRegistration,corporateName,tradeName&userId="+$.cookie('IPI').split('UsuarioGUID=')[1].split('&UrlReferrer')[0],
                    "method": "GET"
                  }
          
                  $.ajax(user).done(function (user) {
                    for (var i = user.length - 1; i >= 0; i--) {
                      var data = user[i].birthDate;
                      if(data != null){
                        $('#data-account').val(data.substring(8,10)+'/'+data.substring(5,7)+'/'+data.substring(0,4));
                      }
                      $('#email-account').val(user[i].email);
                      $('#cpf-account').val(user[i].document);
                      $('#nome-account').val(user[i].firstName);
                      $('#sobrenome-account').val(user[i].lastName);
                      if(user[i].gender == 'M'){
                        $('#sexo-masculino').trigger('click')
                      }else if(user[i].gender == 'F'){
                        $('#sexo-feminino').trigger('click')
                      }
                      $('#telefone-account').val(user[i].phone.replace('+55', ''));
                      $('#razao-account').val(user[i].corporateName);
                      $('#fantasia-account').val(user[i].tradeName);
                      $('#cnpj-account').val(user[i].cnpj);
                      $('#ie-account').val(user[i].stateRegistration);
                      setTimeout(function(){
                        maskara();
                      }, 1000);
                    }
                  });
          
                $('.pessoa-fisica form').submit(function(event) {
                  event.preventDefault();
          
                  if($('#data-account').val() != ''){
                      var mes = $('#data-account').val().substring(3,5),
                        dia = $('#data-account').val().substring(0,2),
                        ano = $('#data-account').val().substring(6,10);
                    if($('[name="sexo"]:checked').val() != ''){
                      var obj_cliente = {
                        "firstName": $('#nome-account').val(),
                        "lastName": $('#sobrenome-account').val(),
                        "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                        "phone" : '+55'+$('#telefone-account').val() ,
                        "email" : $('#email-account').val(),
                        "birthDate" : mes+'/'+dia+'/'+ano,
                        "gender" : $('[name="sexo"]:checked').val()
                      }
          
                    }else{
                      var obj_cliente = {
                        "firstName": $('#nome-account').val(),
                        "lastName": $('#sobrenome-account').val(),
                        "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                        "phone" : '+55'+$('#telefone-account').val() ,
                        "email" : $('#email-account').val(),
                        "birthDate" : mes+'/'+dia+'/'+ano
                      }
                    }
          
                  }else{
                    if($('[name="sexo"]:checked').val()){
                      var obj_cliente = {
                        "firstName": $('#nome-account').val(),
                        "lastName": $('#sobrenome-account').val(),
                        "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                        "phone" : '+55'+$('#telefone-account').val() ,
                        "email" : $('#email-account').val(),
                        "gender" : $('[name="sexo"]:checked').val()
                      }
                    }else{
                      var obj_cliente = {
                        "firstName": $('#nome-account').val(),
                        "lastName": $('#sobrenome-account').val(),
                        "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                        "phone" : '+55'+$('#telefone-account').val() ,
                        "email" : $('#email-account').val(),
                      }
          
                    }
                  }
                  var json_cliente = JSON.stringify(obj_cliente);
                    if(TestaCPF($('#cpf-account').val().replace(/[^\d]+/g,''))){
                      if($('#telefone-account').val().length > 13){
                        insertMasterData("CL", 'vet', json_cliente, function(res) {
                          console.log(res);
                            swal('Dados atualizados com sucesso')
                            location.reload();
                        });
                      }else{
                        swal('Por favor preencha um telefone válido')
                      }
                    }else{
                      swal('Por favor preencha um CPF valido')
                    }
                });
              }
          
              $('.pessoa-fisica form').submit(function(event) {
                  event.preventDefault();
          
                  if($('#data-account').val() != ''){
                          var mes = $('#data-account').val().substring(3,5),
                              dia = $('#data-account').val().substring(0,2),
                              ano = $('#data-account').val().substring(6,10);
                      if($('[name="sexo"]:checked').val() != ''){
                          var obj_cliente = {
                              "firstName": $('#nome-account').val(),
                              "lastName": $('#sobrenome-account').val(),
                              "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                              "phone" : '+55'+$('#telefone-account').val(),
                              "email" : $('#email-account').val(),
                              "birthDate" : mes+'/'+dia+'/'+ano,
                              "gender" : $('[name="sexo"]:checked').val()
                          }
          
                      }else{
                          var obj_cliente = {
                              "firstName": $('#nome-account').val(),
                              "lastName": $('#sobrenome-account').val(),
                              "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                              "phone" : '+55'+$('#telefone-account').val(),
                              "email" : $('#email-account').val(),
                              "birthDate" : mes+'/'+dia+'/'+ano
                          }
                      }
          
                  }else{
                      if($('[name="sexo"]:checked').val()){
                          var obj_cliente = {
                              "firstName": $('#nome-account').val(),
                              "lastName": $('#sobrenome-account').val(),
                              "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                              "phone" : '+55'+$('#telefone-account').val(),
                              "email" : $('#email-account').val(),
                              "gender" : $('[name="sexo"]:checked').val()
                          }
                      }else{
                          var obj_cliente = {
                              "firstName": $('#nome-account').val(),
                              "lastName": $('#sobrenome-account').val(),
                              "document" : $('#cpf-account').val().replace(/[^\d]+/g,''),
                              "phone" : '+55'+$('#telefone-account').val(),
                              "email" : $('#email-account').val(),
                          }
                      }
                  }
                var json_cliente = JSON.stringify(obj_cliente);
                  if(TestaCPF($('#cpf-account').val().replace(/[^\d]+/g,''))){
                      if($('#telefone-account').val().length > 13){
                          insertMasterData("CL", 'mykhome', json_cliente, function(res) {
                              console.log(res);
                                  swal('Dados atualizados com sucesso')    
                          });
                      }else{
                          swal('Por favor preencha um telefone válido')    
                      }
                  }else{    
                      swal('Por favor preencha um CPF valido')    
                  }
              });
            }
        
            maskara();
        
          }

        //INSTITUCIONAL
        if ($('body.institucional').length === 1) {
            // insere formulario de contato no MASTER DATA
            $('.formulario-contato-institucional').submit(function (event) {
                event.preventDefault();

                var nome = $('.formulario-contato-institucional [name="nome"]').val();
                var email = $('.formulario-contato-institucional [name="email"]').val();
                var assunto = $('.formulario-contato-institucional [name="assunto-lista"]').val();
                var telefone = $('.formulario-contato-institucional [name="telefone"]').val();
                var mensagem = $('.formulario-contato-institucional .textarea-mensagem').val();
                var remetente = $('.formulario-contato-institucional [name="remetente"]').val();

                var obj_dados = {
                    "nome": nome,
                    "email": email,
                    "assunto": assunto,
                    "telefone": telefone,
                    "mensagem": mensagem,
                    "remetente": remetente,
                }

                var json_dados = JSON.stringify(obj_dados);

                insertMasterData("FC", 'mykhome', json_dados, function (res) {
                    setTimeout(function () {
                        $('.formulario-contato-institucional').html('<div class="msg_sucesso text-center fw-400"><p>Sua mensagem foi enviada com sucesso!<br/>Em breve você receberá um retorno da nossa equipe de atendimento.</p></div>');
                    }, 1000);
                });
            });
        }

        //CHANGE VALUE 
        $('#newsletterButtonOK').val('CADASTRAR');

        //REMOVE LI VAZIO - PRATELEIRA
        $('.prateleira li').each(function () {
            if ($(this).find('img').length === 0) {
                $(this).remove();
            }
        });

        var best_count = $('#bestselling .grid__item').length;
        if (best_count > 3) {
            $('.nav_bestselling').css('display', 'block');
        } else {
            $('.nav_bestselling').css('display', 'none');
        }
        var bestselling = $("#bestselling ul");
        bestselling.owlCarousel({
            items: 4,
            stagePadding: 50,
            margin: 10,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [980, 3],
            itemsTablet: [630, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            pagination: false
        });

        // Custom Navigation Events
        $(".nav_bestselling .next").click(function () {
            bestselling.trigger('owl.next');
        })

        $(".nav_bestselling .prev").click(function () {
            bestselling.trigger('owl.prev');
        })

        //SECTION 1
        var section_count = $('.section-1 .grid__item').length;
        if (section_count > 3) {
            $('.section-1 .nav_section_1').css('display', 'block');
        } else {
            $('.section-1 .nav_section_1').css('display', 'none');
        }
        var section_1 = $(".section-1 .prateleira ul");
        section_1.owlCarousel({
            items: 4,
            stagePadding: 50,
            margin: 10,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [980, 3],
            itemsTablet: [630, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            pagination: true
        });

        // Custom Navigation Events
        $(".section-1 .nav_section_1 .next").click(function () {
            section_1.trigger('owl.next');
        });

        $(".section-1 .nav_section_1 .prev").click(function () {
            section_1.trigger('owl.prev');
        });

        //SECTION 5
        var section_count_5 = $('.section-5 .grid__item').length;
        if (section_count_5 > 3) {
            $('.nav_section_5').css('display', 'block');
        } else {
            $('.nav_section_5').css('display', 'none');
        }
        var section_5 = $(".section-5 .prateleira ul");
        section_5.owlCarousel({
            items: 4,
            stagePadding: 50,
            margin: 10,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [980, 3],
            itemsTablet: [630, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            pagination: true
        });

        // Custom Navigation Events
        $(".nav_section_5 .next").click(function () {
            section_5.trigger('owl.next');
        });

        $(".nav_section_5 .prev").click(function () {
            section_5.trigger('owl.prev');
        });


        var featured_count = $('#featured .grid__item').length;
        if (featured_count > 3) {
            $('.nav_featured').css('display', 'block');
        } else {
            $('.nav_featured').css('display', 'none');
        }
        var featured = $("#featured ul");
        featured.owlCarousel({
            items: 4,
            stagePadding: 50,
            margin: 10,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [980, 3],
            itemsTablet: [630, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            pagination: false
        });

        // Custom Navigation Events
        $(".nav_featured .next").click(function () {
            featured.trigger('owl.next');
        })

        $(".nav_featured .prev").click(function () {
            featured.trigger('owl.prev');
        })

        var blog_count = $('#blog-carousel .grid__item').length;
        if (blog_count > 3) {
            $('.nav_blogpost').css('display', 'block');
        } else {
            $('.nav_blogpost').css('display', 'none');
        }

        var blogPost = $("#blog-carousel");
        blogPost.owlCarousel({
            items: 4,
            stagePadding: 50,
            margin: 10,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [980, 3],
            itemsTablet: [630, 2],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            pagination: false
        });

        // Custom Navigation Events
        $(".nav_blogpost .next").click(function () {
            blogPost.trigger('owl.next');
        })

        $(".nav_blogpost .prev").click(function () {
            blogPost.trigger('owl.prev');
        })

        var bestselling_count = $('#best_incollection .grid__item').length;
        if (bestselling_count > 3) {
            $('.nav_best_incollection').css('display', 'block');
        } else {
            $('.nav_best_incollection').css('display', 'none');
        }
        if ($("#best_incollection").length > 0) {
            var bestselling = $("#best_incollection");
            bestselling.owlCarousel({
                items: 4,
                stagePadding: 50,
                margin: 10,
                itemsCustom: false,
                itemsDesktop: [1199, 4],
                itemsDesktopSmall: [980, 3],
                itemsTablet: [630, 2],
                itemsTabletSmall: false,
                itemsMobile: [479, 2],
                singleItem: false,
                itemsScaleUp: false,
                responsive: true,
                responsiveRefreshRate: 200,
                responsiveBaseWidth: window,
                autoPlay: false,
                stopOnHover: false,
                navigation: false,
                pagination: false
            });

            // Custom Navigation Events
            $(".nav_best_incollection .next").click(function () {
                bestselling.trigger('owl.next');
            })
            $(".nav_best_incollection .prev").click(function () {
                bestselling.trigger('owl.prev');
            })
        }

        $('#welcome-slide').owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });

        var testimonial_count = $('#testimonial li').length;
        if (testimonial_count > 2) {
            var testimonial = $("#testimonial");
            testimonial.owlCarousel({
                items: 2, //10 items above 1000px browser width
                itemsCustom: false,
                itemsDesktop: [1199, 2],
                itemsDesktopSmall: [980, 2],
                itemsTablet: [630, 2],
                itemsTabletSmall: false,
                itemsMobile: [479, 1],
                singleItem: false,
                itemsScaleUp: false,
                responsive: true,
                responsiveRefreshRate: 200,
                responsiveBaseWidth: window,
                autoPlay: false,
                stopOnHover: false,
                navigation: false,
                pagination: false
            });

            $(".nav_testimonial .next").click(function () {
                testimonial.trigger('owl.next');
            })

            $(".nav_testimonial .prev").click(function () {
                testimonial.trigger('owl.prev');
            })
        }

        //BANNER PRINCIPAL
        $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                $('body').removeClass('slider-loading');
            }
        });

        //HEADER FIXED
        var w = window.innerWidth;
        if (w > 767) {
            if ($('body.account').length === 1) {

            } else {
                $(".header-sticky").sticky({
                    topSpacing: 0
                });
                var headerH = $('.header-sticky').height();
                $(document).bind('ready scroll', function () {
                    var docScroll = $(document).scrollTop();
                    if (docScroll >= headerH) {
                    } else {
                        $('#header-landing').removeClass('header-animate').removeAttr('style');
                    }
                });
            }
        }
        if (w < 767) {
            $('.desktop-megamenu').remove();
        }

        function busca() {
            $('#menu-container .inline').on('click', function (e) {
                e.preventDefault();
                $('.box-search').slideToggle();
            });
        }
        busca();

        function menuMobile () {
            if ($(document).width() < 768) {
                $('.row-3 ul li span').on('click', function(e){
                    if ($(this).next('div').length) {
                        e.preventDefault();
                        
                        if ($(this).next('div').hasClass('active')) {
                            $(this).next('div').removeClass('active');
                            $(this).next('div').slideUp();
                        } else {
                            $('.row-3 ul li span + div').removeClass('active');
                            $('.row-3 ul li span + div').slideUp();

                            $(this).next('div').addClass('active');
                            $(this).next('div').slideDown();
                        }
                        
                    } else {
                        console.log('n tem');
                    }
                });

                $('.hamburger').on('click',function () {
                    $('header .row-3').slideToggle();
                    $('#overlay, body').toggleClass('active');
                });
            }
        } menuMobile();
    });

    e(document).on("click touchstart", function (n) {
        var r = e(".quick-view");
        var i = e("#slidedown-cart");
        var s = e("#ToggleDown");
        var o = e("#email-modal .modal-window");

        if (!r.is(n.target) && r.has(n.target).length === 0 && !i.is(n.target) && i.has(n.target).length === 0 && !s.is(n.target) && s.has(n.target).length === 0 && !o.is(n.target) && o.has(n.target).length === 0) {
            t.closeQuickViewPopup();
            t.closeDropdownCart();
            t.closeEmailModalWindow();

        }
    })

    e(document).keyup(function (n) {
        if (n.keyCode == 27) {
            t.closeQuickViewPopup();
            t.closeDropdownCart();
            clearTimeout(t.KidsTimeout);
            if (e(".modal").is(":visible")) {
                e(".modal").fadeOut(500)
            }
        }
    });
    var t = {
        KidsTimeout: null,
        isSidebarAjaxClick: false,
        init: function () {
            this.initQuickView();
            this.initAddToCart();
            this.initModal();
            this.initProductAddToCart();
            this.initDropDownCart();
            this.initWishlist();
            this.initProductWishlist();
            this.initProductMoreview();
            this.initelevateZoom();

        },
        initWishlist: function () {
            e("button.wishlist").click(function (n) {
                n.preventDefault();
                var r = e(this).parent();
                var i = r.parents(".item-row");
                e.ajax({
                    type: "POST",
                    url: "/contact",
                    data: r.serialize(),
                    beforeSend: function () {
                        t.showLoading()
                    },
                    success: function (n) {
                        t.hideLoading();
                        var o = i.find(".product-thumb img.product-main-img").attr("src");
                        e(".ajax-success-modal").find(".added-to-wishlist").show();
                        e(".ajax-success-modal").find(".added-to-cart").hide();
                        e(".ajax-success-modal").find(".ajax-product-image").attr("src", o);
                        t.showModal(".ajax-success-modal");
                    },
                    error: function (n, r) {
                        t.hideLoading();
                        e(".loading-modal").hide();
                        e(".ajax-error-message").text(e.parseJSON(n.responseText).description);
                        t.showModal(".ajax-error-modal");
                    }
                })
            })
        },
        initelevateZoom: function () {
            if (e("#product-featured-image").length > 0) {
                if ($(window).width() < 768) {
                    e("#product-featured-image").elevateZoom({
                        gallery: 'ProductThumbs',
                        cursor: 'pointer',
                        galleryActiveClass: 'active',
                        imageCrossfade: false,
                        scrollZoom: false,
                        onImageSwapComplete: function () {
                            e(".zoomWrapper div").hide();
                        },
                        loadingIcon: window.loading_url
                    });
                    e("#product-featured-image").bind("click", function (t) {
                        return false;
                    });
                } else {
                    e("#product-featured-image").elevateZoom({
                        gallery: 'ProductThumbs',
                        cursor: 'pointer',
                        galleryActiveClass: 'active',
                        imageCrossfade: true,
                        scrollZoom: true,
                        onImageSwapComplete: function () {
                            e(".zoomWrapper div").hide();
                        },
                        loadingIcon: window.loading_url
                    });

                    e("#product-featured-image").bind("click", function (t) {
                        var ez = e('#product-featured-image').data('elevateZoom');
                        e.fancybox(ez.getGalleryList());
                        return false;
                    });
                }
            }
        },
        initProductWishlist: function () {
            e(".product-detail-section button.wishlist").click(function (n) {
                n.preventDefault();
                var r = e(this).parent();
                var i = r.parents(".grid__item");
                e.ajax({
                    type: "POST",
                    url: "/contact",
                    data: r.serialize(),
                    beforeSend: function () {
                        t.showLoading()
                    },
                    success: function (n) {
                        t.hideLoading();
                        var o = e("#product-featured-image").attr("src");
                        e(".ajax-success-modal").find(".added-to-wishlist").show();
                        e(".ajax-success-modal").find(".added-to-cart").hide();
                        e(".ajax-success-modal").find(".ajax-product-image").attr("src", o);
                        t.showModal(".ajax-success-modal")
                    },
                    error: function (n, r) {
                        t.hideLoading();
                        e(".loading-modal").hide();
                        e(".ajax-error-message").text(e.parseJSON(n.responseText).description);
                        t.showModal(".ajax-error-modal")
                    }
                })
            })
        },

        showModal: function (n) {
            e(n).fadeIn(500);
            t.KidsTimeout = setTimeout(function () {
                e(n).fadeOut(500)
            }, 5e3)
        },
        checkItemsInDropdownCart: function () {
            if (e("#slidedown-cart .mini-products-list").children().length > 0) {
                e("#slidedown-cart .no-items").hide();
                e("#slidedown-cart .has-items").show()
            } else {
                e("#slidedown-cart .has-items").hide();
                e("#slidedown-cart .no-items").show()
            }
        },
        initModal: function () {
            e(".continue-shopping").click(function () {
                clearTimeout(t.KidsTimeout);
                e(".ajax-success-modal").fadeOut(500)
            });
            e(".close-modal, .overlay").click(function () {
                clearTimeout(t.KidsTimeout);
                e(".modal").fadeOut(500)
            })
        },
        initDropDownCart: function () {
            if (window.dropdowncart_type == "click") {
                e("#ToggleDown").click(function () {
                    if (e("#slidedown-cart").is(":visible")) {
                        e("#slidedown-cart").slideUp("fast")
                    } else {
                        e("#slidedown-cart").slideDown("fast")
                    }
                })
            } else {
                if (!("ontouchstart" in document)) {
                    e("#ToggleDown").hover(function () {
                        if (!e("#slidedown-cart").is(":visible")) {
                            e("#slidedown-cart").slideDown("fast")
                        }
                    });
                    e(".wrapper-top-cart").mouseleave(function () {
                        e("#slidedown-cart").slideUp("fast")
                    })
                } else {
                    e("#ToggleDown").click(function () {
                        if (e("#slidedown-cart").is(":visible")) {
                            e("#slidedown-cart").slideUp("fast")
                        } else {
                            e("#slidedown-cart").slideDown("fast")
                        }
                    })
                }
            }
            t.checkItemsInDropdownCart();
            e("#slidedown-cart .no-items a").click(function () {
                e("#slidedown-cart").slideUp("fast")
            });
            e("#slidedown-cart .btn-remove").click(function (n) {
                n.preventDefault();
                var r = e(this).parents(".item").attr("id");
                r = r.match(/\d+/g);
                Shopify.removeItem(r, function (e) {
                    t.doUpdateDropdownCart(e)
                })
            })
        },
        closeDropdownCart: function () {
            if (e("#slidedown-cart").is(":visible")) {
                e("#slidedown-cart").slideUp("fast")
            }
        },

        initProductAddToCart: function () {
            if (e("#AddToCart").length > 0) {
                e("#AddToCart").click(function (n) {
                    n.preventDefault();
                    if (e(this).attr("disabled") != "disabled") {
                        if (!window.ajax_cart) {
                            e(this).closest("form").submit()
                        } else {
                            var r = e("#AddToCartForm select[name=id]").val();
                            if (!r) {
                                r = e("#AddToCartForm input[name=id]").val()
                            }
                            var i = e("#AddToCartForm input[name=quantity]").val();
                            if (!i) {
                                i = 1
                            }
                            var o = e("#product-featured-image").attr("src");
                            t.doAjaxAddToCart(r, i, o)
                        }
                    }
                    return false
                })
            }
        },
        initAddToCart: function () {
            if (e(".add-cart-btn").length > 0) {
                e(".add-cart-btn").click(function (n) {
                    n.preventDefault();
                    if (e(this).attr("disabled") != "disabled") {
                        var r = e(this).parents(".item-row");
                        var i = e(r).attr("id");
                        i = i.match(/\d+/g);
                        if (!window.ajax_cart) {
                            e("#cart-form-" + i).submit()
                        } else {
                            var s = e("#cart-form-" + i + " select[name=id]").val();
                            if (!s) {
                                s = e("#cart-form-" + i + " input[name=id]").val()
                            }
                            var o = e("#cart-form-" + i + " input[name=quantity]").val();
                            if (!o) {
                                o = 1
                            }
                            var a = e(r).find(".product-thumb img.product-main-img").attr("src");
                            t.doAjaxAddToCart(s, o, a)
                        }
                    }
                    return false
                })
            }
        },
        showLoading: function () {
            e(".loading-modal").show()
        },
        hideLoading: function () {
            e(".loading-modal").hide()
        },
        doAjaxAddToCart: function (n, r, a) {
            e.ajax({
                type: "post",
                url: "/cart/add.js",
                data: "quantity=" + r + "&id=" + n,
                dataType: "json",
                beforeSend: function () {
                    t.showLoading()
                },
                success: function (n) {
                    t.hideLoading();
                    t.showModal(".ajax-success-modal");
                    e(".ajax-success-modal").find(".ajax-product-image").attr("src", a);
                    e(".ajax-success-modal").find(".added-to-wishlist").hide();
                    e(".ajax-success-modal").find(".added-to-cart").show();
                    t.updateDropdownCart()
                },
                error: function (n, r) {
                    t.hideLoading();
                    e(".ajax-error-message").text(e.parseJSON(n.responseText).description);
                    t.showModal(".ajax-error-modal")
                }
            })
        },
        initQuickView: function () {
            e(".quick-view-text").click(function () {
                var n = e(this).attr("id");
                Shopify.getProduct(n, function (n) {
                    var r = e("#quickview-template").html();
                    e(".quick-view").html(r);
                    var i = e(".quick-view");
                    var s = n.description.replace(/(<([^>]+)>)/ig, "");
                    s = s.split(" ").splice(0, 30).join(" ") + "...";
                    i.find(".product-title a").text(n.title);
                    i.find(".product-title a").attr("href", n.url);
                    if (i.find(".product-inventory span").length > 0) {
                        var o = n.variants[0].inventory_quantity;
                        if (o > 0) {
                            if (n.variants[0].inventory_management != null) {
                                i.find(".product-inventory span").text(o + " in stock")
                            } else {
                                i.find(".product-inventory span").text("Many in stock")
                            }
                        } else {
                            i.find(".product-inventory span").text("Out of stock")
                        }
                    }
                    i.find(".product-description").text(s);
                    i.find(".price").html(Shopify.formatMoney(n.price, window.money_format));
                    i.find(".product-item").attr("id", "product-" + n.id);
                    i.find(".variants").attr("id", "product-actions-" + n.id);
                    i.find(".variants select").attr("id", "product-select-" + n.id);
                    if (n.compare_at_price > n.price) {
                        i.find(".compare-price").html(Shopify.formatMoney(n.compare_at_price_max, window.money_format)).show();
                        i.find(".price").addClass("on-sale")
                    } else {
                        i.find(".compare-price").html("");
                        i.find(".price").removeClass("on-sale")
                    }
                    if (!n.available) {
                        i.find("select, input, .total-price, .dec, .inc, .variants label").remove();
                        i.find(".add-cart-btn").text("Unavailable").addClass("disabled").attr("disabled", "disabled");
                    } else {
                        i.find(".total-price .price").html(Shopify.formatMoney(n.price, window.money_format));
                        t.createQuickViewVariants(n, i)

                    }
                    i.find(".button").on("click", function () {
                        var n = i.find(".quantity").val(),
                            r = 1;
                        if (e(this).text() == "+") {
                            r = parseInt(n) + 1
                        } else if (n > 1) {
                            r = parseInt(n) - 1
                        }
                        i.find(".quantity").val(r);
                    });

                    t.loadQuickViewSlider(n, i);
                    t.initQuickviewAddToCart();
                    e(".quick-view").fadeIn(500);

                });
                return false
            });
            e(".quick-view .overlay, .close-window").live("click", function () {
                t.closeQuickViewPopup();
                return false
            })

        },

        initQuickviewAddToCart: function () {
            if (e(".quick-view .AddToCart").length > 0) {
                e(".quick-view .AddToCart").click(function () {
                    var n = e(".quick-view select[name=id]").val();
                    if (!n) {
                        n = e(".quick-view input[name=id]").val()
                    }
                    var r = e(".quick-view input[name=quantity]").val();
                    if (!r) {
                        r = 1
                    }

                    var a = e(".quick-view .quickview-featured-image img").attr("src");
                    t.doAjaxAddToCart(n, r, a);
                    t.closeQuickViewPopup()
                })
            }
        },
        updateDropdownCart: function () {
            Shopify.getCart(function (e) {
                t.doUpdateDropdownCart(e)
            })
        },
        doUpdateDropdownCart: function (n) {
            var r = '<li class="item" id="cart-item-{ID}"><a href="{URL}" title="{TITLE}" class="product-image"><img src="{IMAGE}" alt="{TITLE}"></a><div class="product-details"><p class="product-name"><a href="{URL}">{TITLE}</a></p><div class="cart-collateral">{QUANTITY} x <span class="price">{PRICE}</span></div></div><a href="javascript:void(0)" title="Remove This Item" class="btn-remove"><span class="icon icon-Delete"></span></a></li>';
            e("#cartCount").text(n.item_count);
            e("#slidedown-cart .summary .price").html(Shopify.formatMoney(n.total_price, window.money_format));
            e("#slidedown-cart .mini-products-list").html("");
            if (n.item_count > 0) {
                for (var i = 0; i < n.items.length; i++) {
                    var s = r;
                    s = s.replace(/\{ID\}/g, n.items[i].id);
                    s = s.replace(/\{URL\}/g, n.items[i].url);
                    s = s.replace(/\{TITLE\}/g, n.items[i].title);
                    s = s.replace(/\{QUANTITY\}/g, n.items[i].quantity);
                    s = s.replace(/\{IMAGE\}/g, Shopify.resizeImage(n.items[i].image, "small"));
                    s = s.replace(/\{PRICE\}/g, Shopify.formatMoney(n.items[i].price, window.money_format));
                    e("#slidedown-cart .mini-products-list").append(s)
                }
                e("#slidedown-cart .btn-remove").click(function (n) {
                    n.preventDefault();
                    var r = e(this).parents(".item").attr("id");
                    r = r.match(/\d+/g);
                    Shopify.removeItem(r, function (e) {
                        t.doUpdateDropdownCart(e)
                    })
                });
                if (t.checkNeedToConvertCurrency()) {
                    Currency.convertAll(window.shop_currency, jQuery("#currencies").val(), "#slidedown-cart span.money", "money_format")
                }
            }
            t.checkItemsInDropdownCart()
        },
        checkNeedToConvertCurrency: function () {
            return window.show_multiple_currencies && Currency.currentCurrency != shopCurrency
        },
        loadQuickViewSlider: function (n, r) {
            var s = Shopify.resizeImage(n.featured_image, "large");
            r.find(".quickview-featured-image").append('<a href="' + n.url + '"><img src="' + s + '" title="' + n.title + '"/><div style="height: 100%; width: 100%; top:0px; left:-260px; z-index: 2000; position: absolute; display: none; background: url(' + window.loading_url + ') 50% 50% no-repeat;"></div></a>');
            if (n.images.length > 1) {
                var o = r.find(".more-view-wrapper ul");
                for (i in n.images) {
                    var u = Shopify.resizeImage(n.images[i], "large");
                    var a = Shopify.resizeImage(n.images[i], "small");
                    var f = '<li><a href="javascript:void(0)" data-image="' + u + '"><img src="' + a + '"  /></a></li>';
                    o.append(f)
                }
                o.find("a").click(function () {
                    var t = r.find(".quickview-featured-image img");
                    var n = r.find(".quickview-featured-image div");
                    if (t.attr("src") != e(this).attr("data-image")) {
                        t.attr("src", e(this).attr("data-image"));
                        n.show();
                        t.load(function (t) {
                            n.hide();
                            e(this).unbind("load");
                            n.hide()
                        })
                    }
                });

                if (o.hasClass("quickview-more-views-owlslider")) {
                    t.initQuickViewMoreview(o)
                } else {
                    t.initQuickViewMoreview(o)
                }

            } else {

                r.find(".more-view-wrapper").remove();

            }
        },
        closeEmailModalWindow: function () {
            if (e("#email-modal").length > 0 && e("#email-modal").is(":visible")) {
                e("#email-modal .modal-window").fadeOut(600, function () {
                    e("#email-modal .modal-overlay").fadeOut(600, function () {
                        e("#email-modal").hide();
                        e.cookie("emailSubcribeModal", "closed", {
                            expires: 1,
                            path: "/"
                        })
                    })
                })
            }
        },
        initQuickViewMoreview: function (t) {
            if (t) {
                t.owlCarousel({
                    navigation: true,
                    items: 4,
                    itemsDesktop: [1199, 4],
                    itemsDesktopSmall: [979, 4],
                    itemsTablet: [768, 3],
                    itemsTabletSmall: [540, 3],
                    itemsMobile: [360, 3]
                }).css("visibility", "visible")
            }
        },


        convertToSlug: function (e) {
            return e.toLowerCase().replace(/[^a-z0-9 -]/g, "").replace(/\s+/g, "-").replace(/-+/g, "-")
        },

        createQuickViewVariants: function (t, n) {
            if (t.variants.length > 1) {
                for (var r = 0; r < t.variants.length; r++) {
                    var i = t.variants[r];
                    var s = '<option value="' + i.id + '">' + i.title + "</option>";
                    n.find("form.variants > select").append(s)
                }
                new Shopify.OptionSelectors("product-select-" + t.id, {
                    product: t,
                    onVariantSelected: selectCallbackQuickview
                });

                if (t.options.length == 1) {
                    e(".selector-wrapper:eq(0)").prepend("<label>" + t.options[0].name + "</label>")
                }
                n.find("form.variants .selector-wrapper label").each(function (n, r) {
                    e(this).html(t.options[n].name)
                })
            } else {
                n.find("form.variants > select").remove();
                var o = '<input type="hidden" name="id" value="' + t.variants[0].id + '">';
                n.find("form.variants").append(o)
            }
        },
        initProductMoreview: function () {
            e(".more-view-wrapper-owlslider ul").owlCarousel({
                navigation: true,
                items: 7,
                itemsDesktop: [1199, 7],
                itemsDesktopSmall: [979, 4],
                itemsTablet: [768, 4],
                itemsTabletSmall: [540, 4],
                itemsMobile: [360, 3]
            }).css("visibility", "visible")

        },
        closeQuickViewPopup: function () {
            e(".quick-view").fadeOut(500)
        }

    }
})(jQuery)